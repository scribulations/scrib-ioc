Scrib IoC
=========

## Overview

Scrib IoC is a very simple *quasi* Inversion of Control/Dependency Injection container designed to work with Unity. The main purpose for developing it, considering much more robust and proven solutions are available, is to have a simple solution to package with assets on the Unity Asset store.

### The Problem

#### Service Oriented Architecture and Inversion of Control Issues within Unity

Unity has a very flexible scripting engine. Part of this flexibility comes from the fact that Unity manages all Game Object instantiation. This is great because it simplifies the development pipeline allowing the user to drag and drop items in the GUI and the engine itself handles the instantiation of these element. However, for those of us who like to use Service Oriented Architecture (SOA) approaches to our development we lack the ability to use the most common method of Dependency Injection (DI) which is Constructor Injection. It also removes the ability to be unobtrusive with Setter Injection since instantiation is controlled by the Unity Engine and therefore we can't inject values into objects upon creation without them initiating the injection.

What all this means is that there is no way to managed objects ignorant to what container is injecting into them. Because of this not only is Service Oriented Architecture a difficult design approach to take but it is simply impossible to implement a proper Inversion of Control container since the objects themselves always have to know about the container which is unwanted coupling.

##### Distribution on the Asset Store

Primarily I use StrangeIoC as my framework of choice in Unity. It's a very well developed and powerful framework that offers far more features then a simple IoC/DI container. However, this is 3rd party code and if I am going to distribute assets on the store I don't want to add unneeded features to the assets but also want to remove reliance on 3rd party code due to legal issues as well as the potential to create conflicts within existing projects.

### The Solution

Enter Scrib IoC. The intent of this project is to create a very simple, self contained, IoC framework that can be used within assets so they can be usable out of the box but also allow more advanced users the ability to extend and reference the services provided by the assets without too much trouble.

## Features
### Implemented
- Provides lifetime scoping (Singleton, Concrete).
- Framework for Factory support.
- Context scoping is achieved by virtue of each managed class needing to initiate the dependency injection process theme selves.

### Planned
- Build out Factory support.
- The ability to have multiple context scopes.

## Usage

### Defining the Context

Usage is fairly simple. The context is defined within a script attached to a GameObject that instantiates the Dependency Injection Container as well as configures the context:

    namespace Scribulations.IoC.Example {
        class DIBootstrap : MonoBehaviour {
            private IDIContainer container;

            void Awake() {
                container = new DIContainer();
                container.Bind<ITestService>().ToConcrete<TestService>();
                container.Bind<ITestService>("Singleton").ToSingleton<TestService>();
                container.Bind<ITestService>("Factory").ToFactory(new TestServiceFactor());
            }
        }
    }

Notes:

- The context must be defined with the Awake method. This is due to Unity's Execution Order and the fact that the context must be defined prior to any dependencies being resolved. If need be the context can be defined with Start(), however, the project would have to be configured to ensure that the bootstrap script is the first to run.
- The Dependency Injection Container (IDIContaienr container) can have any access level. Currently it uses a singleton to manage its own state and to allow the injector access to it.
- container.Bind<>() Creates a binding point within the container.
- The ToConcrete method creates a target for that binding. Also available are ToSingleton and ToFactory methods if you want bind to a singleton or factory.
- The string parameter passed into Bind denotes an identifier that can be used to further differentiate between bindings. For instance it may be the case that you have multiple implementations of an interface and therefore would require an additional level of delineation in order to specify which dependency you need. Also note that this parameter can be ignored which creates the default binding which will be used when ever an identifier is not specified.

### Injecting

Injection is very simple. You apply the Dependency attribute to anything you wish to inject. The container will check its mappings when it attempts to resolve the dependencies and inject the correct instance into the fields.

    namespace Scribulations.IoC.Example {
        class TestInjectionBehaviour : MonoBehaviour  {
            [Dependency] private ITestService testService;
            [Dependency("Singleton")] private ITestService testServiceSingleton;
            [Dependency("Factory")] private ITestService testServiceFromFactory;

            public void Start() {
                this.ResolveDependencies<TestInjectionBehaviour>();
            }

            ...

        }
    }

Notes:

- The [Dependency] attribute is provided by the project and acts as a signal to the injector that the following field requires its dependency to be resolved and injected. the field can
- this.ResolveDependencies<TestInjectionBehaviour>(); is an extension method to the MonoBehaviour class provided by this project. It functions as the Injector in the Dependency Injection design pattern.
- This can also be used for more then MonoBehaviours, however, it is recommended to use Constructor Injection whenever possible in order to both avoid coupling with the DIContainer as well as the performance issues that arise with reflection.
- This is not true Inversion of Control. In a complete solution the consumer should not have to know about the container that's injecting into it. In this case both the Dependency attribute and ResolveDependencies<T>() method become coupling points to the container. However, due to the way Unity works it isn't possible to have a container that knows about the objects it manages without the objects themselves telling it.
- This uses reflection which can be slow, however, once the object is instantiated it's as fast as any other.

## License

    Copyright 2016 Bradley Bertrim

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
