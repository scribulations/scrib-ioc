﻿/* Copyright 2016 Bradley Bertrim
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
** 
**     http://www.apache.org/licenses/LICENSE-2.0
** 
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scribulations.IoC {
    public class DIContainer : IDIContainer {
        Dictionary<Type, Dictionary<string, DIDependency>> dependencyMap = new Dictionary<Type, Dictionary<string, DIDependency>>();                

        public static IDIContainer Instance { get; protected set; }

        public DIContainer() {
            Instance = this;
        }

        /// <summary>
        /// Attempts to resolve the provided dependency given it's interface type.
        /// </summary>
        /// <param name="type">The interface type of the dependency</param>
        /// <returns>An instance of the mapped dependency</returns>
        public object Resolve(Type type) {
            return Resolve(type, String.Empty);
        }

        /// <summary>
        /// Attempts to resolve the provided dependency given it's interface type and an identifier.
        /// </summary>
        /// <param name="type">The interface type of the dependency</param>
        /// <param name="identifier">The identifier for the specific resource</param>
        /// <returns>An instance of the mapped dependency</returns>
        public object Resolve(Type type, string identifier) {
            object instance;
            try {
                DIDependency dIDependency = dependencyMap[type][identifier ?? string.Empty];


                if (dIDependency.IsSingleton) {
                    if (dIDependency.Instance == null) {
                        dIDependency.Instance = Activator.CreateInstance(dIDependency.ConcreteType);
                    }
                    instance = dIDependency.Instance;
                } else if(dIDependency.Factory != null) {
                    instance = dIDependency.Factory.Get();
                } else {
                    instance = Activator.CreateInstance(dIDependency.ConcreteType);
                }
            } catch (KeyNotFoundException ex) {
                throw new ResourceNotFoundException("The requested resource: [" + type.FullName + "][" + identifier + "] was not registered when attempting to resolve in the Dependency Injection Container.");
            }

            return instance;
        }


        /// <summary>
        /// Attempts to resolve the provided dependency given it's interface type.
        /// </summary>
        /// <typeparam name="T">The interface type of the dependency</typeparam>
        /// <returns>An instance of the mapped dependency</returns>
        public T Resolve<T>() {
            return Resolve<T>(string.Empty);
        }

        /// <summary>
        /// Attempts to resolve the provided dependency given it's interface type and an identifier.
        /// </summary>
        /// <typeparam name="T">The interface type of the dependency</typeparam>
        /// <param name="identifier">The identifier for the specific resource</param>
        /// <returns>An instance of the mapped dependency</returns>
        public T Resolve<T>(string identifier) {
            T instance;

            try { 
                DIDependency dIDependency = dependencyMap[typeof(T)][identifier ?? string.Empty];

                if (dIDependency.IsSingleton) {
                    if (dIDependency.Instance == null) {
                        dIDependency.Instance = Activator.CreateInstance(dIDependency.ConcreteType);
                    }
                    instance = (T) dIDependency.Instance;
                }else if (dIDependency.Factory != null) {
                    instance = (T) dIDependency.Factory.Get();
                } else {
                    instance = (T)Activator.CreateInstance(dIDependency.ConcreteType);
                }
            } catch (KeyNotFoundException ex) {
                throw new ResourceNotFoundException("The requested resource: [" + typeof (T).FullName + "][" + identifier + "] was not registered when attempting to resolve in the Dependency Injection Container");
            }

            return instance;
        }

        /// <summary>
        /// Creates a default bind for an interface. Note that the resulting DIDependency will need be 
        /// used to set the bind targed via its ToConcrete(), ToSingleton() or ToFactory()
        /// methods.
        /// </summary>
        /// <typeparam name="T">The type of interface to create the bind.</typeparam>
        /// <returns>The DIDependency that denotes the dependency binding.</returns>
        public DIDependency Bind<T>() {
            return Bind<T>(string.Empty);
        }

        /// <summary>
        /// Creates a bind, identified by the identifier parameter, for an interface. Note 
        /// that the resulting DIDependency will need be used to set the bind targed via its 
        /// ToConcrete(), ToSingleton() or ToFactory() methods.
        /// </summary>
        /// <typeparam name="T">The type of interface to create the bind.</typeparam>
        /// <param name="identifier">The identifier to associate with the dependency.</param>
        /// <returns>The DIDependency that denotes the dependency binding.</returns>
        public DIDependency Bind<T>(string identifier) {
            DIDependency resource = new DIDependency();
            Dictionary<string, DIDependency> resourceList;

            if (dependencyMap.ContainsKey(typeof(T))) {       //If the key exists we get the resource list
                resourceList = dependencyMap[typeof (T)];
            } else {                                        //If it doesn't we create a resource list and add it to the map
                resourceList = new Dictionary<string, DIDependency>();
                dependencyMap.Add(typeof(T), resourceList);
            }

            //Does the resource list contain the specified identifier?
            if (!resourceList.ContainsKey(identifier)) {
                resourceList.Add(identifier, resource);
            }

            return resource;
        }
    }
}
