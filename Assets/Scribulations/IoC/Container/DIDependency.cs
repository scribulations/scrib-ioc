﻿/* Copyright 2016 Bradley Bertrim
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
** 
**     http://www.apache.org/licenses/LICENSE-2.0
** 
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

using System;

namespace Scribulations.IoC {
    public class DIDependency {
        public Type ConcreteType { get; set; }
        public object Instance { get; set; }      
        public IFactory Factory { get; set; }
        public bool IsSingleton { get; set; }

        public void ToConcrete<T>() {
            ToConcrete(typeof(T));
        }

        public void ToConcrete(Type concreteType) {
            this.ConcreteType = concreteType;
            this.Instance = null;
            this.Factory = null;
            this.IsSingleton = false;
        }

        public void ToSingleton<T>() {
            ToSingleton(typeof(T));
        }
    
        public void ToSingleton(Type concreteType) {
            this.ConcreteType = concreteType;
            this.Factory = null;
            this.IsSingleton = true;
        }

        public void ToFactory(IFactory factory) {
            this.Factory = factory;
            this.Instance = null;
            this.ConcreteType = null;
            this.IsSingleton = false;
        }
    }
}
