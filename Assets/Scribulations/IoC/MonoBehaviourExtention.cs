﻿/* Copyright 2016 Bradley Bertrim
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
** 
**     http://www.apache.org/licenses/LICENSE-2.0
** 
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

namespace Scribulations.IoC {
    public static class MonoBehaviourExtention {

        /// <summary>
        /// Resolves all the dependencies in the calling class. Note that we have to use the template type here since
        /// we don't have an extended MonoBehaviour type to work with. The template type will be used for the reflection
        /// since it'll comtain the annotated members we want to resolve.
        /// </summary>
        /// <typeparam name="T">The super type we want to resolve the dependencies for</typeparam>
        /// <param name="monoBehaviour">The base MonoBehaviour object</param>
        public static void ResolveDependencies<T>(this MonoBehaviour monoBehaviour) {
            FieldInfo[] fields = typeof(T).GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (FieldInfo fieldInfo in fields) {
                DependencyAttribute[] dependencyAttributes = (DependencyAttribute[])fieldInfo.GetCustomAttributes(typeof(DependencyAttribute),true);

                foreach (DependencyAttribute dependencyAttribute in dependencyAttributes) {
                    object service = DIContainer.Instance.Resolve(fieldInfo.FieldType, dependencyAttribute.Identifier);
                    fieldInfo.SetValue(monoBehaviour, service);
                }
                
            }
        }
    }
}
