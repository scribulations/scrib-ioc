﻿/* Copyright 2016 Bradley Bertrim
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
** 
**     http://www.apache.org/licenses/LICENSE-2.0
** 
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scribulations.IoC.Example.Factory;
using Assets.Scribulations.IoC.Test;
using UnityEngine;

namespace Scribulations.IoC.Example {
    class DIBootstrap : MonoBehaviour {
        private IDIContainer container;

        void Awake() {
            container = new DIContainer();
            container.Bind<ITestService>().ToConcrete<TestService>();
            container.Bind<ITestService>("Singleton").ToSingleton<TestService2>();
            container.Bind<ITestService>("Factory").ToFactory(new TestServiceFactory());
        }
    }
}
